---
title: Support
---

* Come discuss with us on [IFB Community](https://community.france-bioinformatique.fr/)

* Search for previous issues or ask for help: [IFB Core Cluster community support](https://community.france-bioinformatique.fr/c/ifb-core-cluster/)

* Most of the softwares are public and documentation is widely available on the Internet

* Check the documentation on [IFB Core Cluster documentation](http://taskforce-nncr.gitlab.cluster.france-bioinformatique.fr/doc/).   

* Contact us: [contact-nncr-cluster@groupes.france-bioinformatique.fr](mailto:contact-nncr-cluster@groupes.france-bioinformatique.fr).

