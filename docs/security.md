---
title: Security
---

The IFB Core Cluster is **not** authorized to host **healthcare data**.

L'IFB Core CLuster n'est **pas habilité** pour héberger des **données de santé à caractère personnel** (certification HDS).


## Datacenter

The IFB Core Cluster is hosted by IDRIS (https://www.idris.fr/).

This center has high security standards:  
  - Access is restricted and strictly controlled (closed zone, "Zone à Régime Restrictif"), including access control system, intrusion detection system, video surveillance and security guard  
  - Redundant power supply  
  - Reliable air conditioning system  
  - Fire protection


## Network

Network access is controlled and restricted by a firewall.

All access protocols are encrypted.  
Cluster access protocol is **SSH** (or SFTP).  
Web access is done on **HTTPS**. Certificates are provided by TERENA.


## Authentication

The IFB Core Cluster users are managed through a central directory (OpenLDAP).  
The access is limited to authorized users.

The IFB Core cluster supports password and public key authentication.

Users or IP with multiple wrong access can be banned.

Password must meet minimum requirements.

Some web services (usegalaxy.fr, community.france-bioinformatique.fr, etc) provide their own authentication method and accept anonymous connections.


## Data access

Data access (home directory or project directory) are granted using access-control list (ACL).

Access is managed by users or groups.

A project is a group and a directory on the storage.  
A project can be shared.  
Each access on a project must be approved by the project owner.

Data access are not logged.

IT administrators (IFB core team support) can access the data but only in cases of security or maintenance.


## Backup

There is no backup for the main storage.

Some snapshots are available to protect against deletion by error but only one by day and for 5 days.

All servers and services are deployed using Ansible (and configurations are under revision control).  
Main infrastructure services are backed up.


## Data encryption

There is no encryption on the storage.


## Availability

This service is provided as an academic best effort, but without any warranty.


## Monitoring

IT Infrastructure is monitored and the IFB core team is notified by email on each warning.



## Contact

IFB Cluster TaskForce : contact-nncr-cluster@france-bioinformatique.fr
