# The project concept at IFB

## General issues
To ensure good data lifecycle management with our users, we strive to identify the projects that are processed on the cluster.

We want to avoid large unfinishable project folders with a mess inside (sub-sub-projects).

A project has a perimeter: a subject, a goal, a beginning, an end... A typical project should take place in a window of 1 to 3 years. We can consider the project as a funding grant (obtained or not :/).

Note that if you manage several projects in parallel, it is quite possible for you to request additional project spaces with their own lifetime and list of contributors.

![Data lifecycle schema](imgs/data/Data_lifecycle.png)

([source](https://en.wikipedia.org/wiki/Data_management))

FYI, in the future, we plan to request a DMP for each project request.


## A good project name
The ideal name for a project is its ANR acronyme. But you can compose with:
 - the species
 - the response being studied
 - the technologies or methods involved
The ideal name isn't too long :)

Avoid:
 - your name
 - the name of your laboratory or team
 - anything not relevant to your study

## Slurm accounts and projects

Each project hosted on the cluster is associated with a Slurm account.

NOTE that to submit jobs through Slurm you need at least one Slurm account, so at least one project. The Slurm accounts stick to the project names.

Your first project or the first project you get associated with will become your default account. This means that all your Slurm jobs will be accounted on this project account unless you specify a different account.

The `-A` option in srun or sbatch let you specify which account you want to use.

```bash
srun -A myproject1 hostname
sbatch -A myproject2 awesome_script.sh
```

You can change your default account using the sacctmgr command :

```bash
sacctmgr update user <your-login> set defaultaccount=<project-name>
```