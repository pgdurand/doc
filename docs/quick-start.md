---
title: Quick start guide
category: documentation
i18n-id: quick-start
index: 2
---

# Quick start guide



## Account

Sign up (request an account): [IFB Core Cluster - Account Request](https://my.cluster.france-bioinformatique.fr).    
The account will be created in few days.

More information about services on [IFB Core Cluster website](https://www.france-bioinformatique.fr/fr/cluster).



## Log in

Access: **SSH**  
Server: **core.cluster.france-bioinformatique.fr**

On Linux/Mac, simply use ssh client like [OpenSSH](https://www.openssh.com/)   
```bash
ssh <username>@core.cluster.france-bioinformatique.fr
```    

<asciinema-player src="../casts/connection.cast"  id="asciicast-connection" async poster="npt:0:04" speed="2" preload="true" data-autoplay="false"></asciinema-player>

On Windows, you can use clients like [PuTTY](http://the.earth.li/~sgtatham/putty/latest/x86/putty.exe)    
<a onclick="toggle_visibility('putty');">> Show me</a>
<video id="putty" class="hide" width="572" controls><source src="../casts/putty_connection.mp4" type="video/mp4">Your browser does not support the video tag.</video>

Please see the [Logging in](logging-in.md) page for further details.




## Data

### Storage

Several volumes of data storage are available on the NNCR cluster.    

|                      | Usage                                   | Quota (default)  | Backup policy |
| -------------------- | --------------------------------------- | ----------------:| -------------:|
| **/shared/home**     | Home directory (personnal data)         |            200GB |            NA |
| **/shared/projects** | Scientific and project (common data)    |              1TB |            NA |

* The **/shared/bank** directory contains common banks (*UniProt*, *RefSeq*, ...)  
* If you need or plan to use more storage space, please [contact-us](mailto:contact-nncr-cluster@groupes.france-bioinformatique.fr). 

<!--
* Restoring data (snapshots): 
If you change or remove data by error, you can try to get back your data thanks to snapshots.
Simply browse the .snpashots directory (/shared/home/.snapshots/ or /shared/projects/.snapshots/), sorted by date, and visualize/copy/restore your data. 
-->

Please see "[storage](data.md#storage)" page for further details.



### Transfer

SSH protocol (or SFTP - SSH File Transfer Protocol) is the only protocol available.    
But, you can use many clients to download your data from the cluster (*scp*, *rsync*, *wget*, *ftp*, etc.).

<asciinema-player src="../casts/transfer.cast"  id="asciicast-transfer" async poster="npt:0:11" speed="1.5" preload="true" data-autoplay="false"></asciinema-player>

You can also use graphic clients like [FileZilla](https://filezilla-project.org/).     
<a onclick="toggle_visibility('filezilla');">> Show me</a>
<video id="filezilla" class="hide" width="572" controls><source src="../casts/filezilla_lowquality.mp4" type="video/mp4">Your browser does not support the video tag.</video>

Or simply use your file manager with [SSHFS](https://github.com/libfuse/sshfs)    
<a onclick="toggle_visibility('sshfs');">> Show me</a>
<video id="sshfs" class="hide" width="572" controls><source src="../casts/sshfs.mp4" type="video/mp4">Your browser does not support the video tag.</video>

Please see the [Transfer](data.md#transfer) page for further details.





## Software

To use softwares like blast, python, gcc, etc. we have to "load" them using module commands ([Environment Modules](http://modules.sourceforge.net/)):

* List: `module avail`
* Use blast: `module load blast`
* Use a specific version: `module load blast/2.2.25`

<asciinema-player src="../casts/module.cast"  id="asciicast-module" async poster="npt:0:12" speed="2" preload="true" data-autoplay="false"></asciinema-player>

You can also use singularity or conda directly.

Please see the [Conda](conda.md) page for further details.   
Please see the [Singularity](singularity.md) page for further details.



## Submit a job

The computing works are done by submitting "jobs" to the workload manager [Slurm](https://slurm.schedmd.com/ "It's Highly Addictive!").    
You **must** use Slurm to execute your jobs.

#### 1. Write a bash script
This script must contain your commands to execute.
Many editors are available (see [editors](data.md#editors) page).  
Here, inside `myscript.sh`, we launch a *bowtie* command and just print some truth.  
```bash
#!/bin/bash

bowtie2 -x hg19 -1 sample_R1.fq.gz -2 sample_R2.fq.gz -S sample_hg19.sam

echo "Enjoy slurm ! It's highly addictive."
```

#### 2. Add options and requirements
You can specify several options to your jobs (name, number of CPU, amount of memory, time limit, etc.). All this parameter take place in the beginning of the script with the `#SBATCH` directive (just after the shebang *#!/bin/bash*).    
Here we specify the job name and the amount of memory required.    
**Advice:** We recommend to set as many parameters as you can in the script in order to keep a track of your execution parameters for a future submission.
```bash
#!/bin/bash

#SBATCH --job-name=bowtie
#SBATCH --mem=40GB

bowtie2 -x hg19 -1 sample_R1.fq.gz -2 sample_R2.fq.gz -S sample_hg19.sam

echo "Enjoy slurm ! It's highly addictive."
```

#### 3. Launch your job with `sbatch`
```bash
sbatch myscript.sh
```
The command return a *jobid* to identify your job.  
See more usefull information below ([Slurm commands](quick-start.md#slurm-commands)).  

#### 4. Follow your job
The status goes successively from PENDING (PD) to RUNNING (R) and finally COMPLETED (C) (and job disappear from the queue). So if your job is not displayed, your jobs is finished (with success or with error).
```bash
squeue
```

#### 5. See output
The output of the script (standard output and standard error) is live written.    
Default output file is `slurm-[jobid].out` in your working directory.     
And of course, if you have some result files like `sample_hg19.sam`, these files will be available.


#### Notes

* All nodes have access to the data (*/shared/home*, */shared/projects* or */shared/bank*).

* All softwares are available on the nodes, but we have to load them inside the script with command *module add [module]*.

* All jobs are contained and can not use more resources than defined (CPU, memory).

* Jobs which exceed limits (memory or time, values by default or set) are killed.

* It's possible to be connected on a compute node when a job is running (*ssh cpu-node-XX*).




#### Demo

<asciinema-player src="../casts/submit-job.cast"  id="asciicast-submit-job" async poster="npt:0:14" speed="2" preload="true" data-autoplay="false"></asciinema-player>



#### Slurm commands

If you are used to PBS/Torque/SGE/LSF/LoadLeveler, refer to the [Rosetta Stone of Workload Managers](https://slurm.schedmd.com/rosetta.pdf)

* Submit a job: `sbatch myscript.sh`
* Information on jobs: `squeue`
* Information on my jobs: `squeue -u $USER`
* Information on running job: `scontrol show job <jobid>`
* Delete a job: `scancel <jobid>`

| Options frequently used                  |                                               |
| ---------------------------------------- |---------------------------------------------- |
`--job-name=demojob`      | Job name 
`--time=01:00:00`         | Limit run time "hours:minutes:seconds" (*default = max partition time*) 
`--partition=long`        | Select partition (*default = fast*) <br>Partitions = **fast** (job <= 12 hours) or **long** (job > 12 hours) 
`--nodes=N`               | Request *N* compute node to this job (*default = 1*) 
`--cpus-per-task=N`       | Number of cores/tasks requested (*default = 1 per node*) 
`--mem=2GB`<br>`--mem-per-cpu=2GB` | Amount of real memory (*default = 2GB/cpu*) 
`--exclusive`             | Whole node only for you 
`--output=slurm-%j.out`   | Specify the output file (standard output and error, *default = slurm-[jobid].out*) 
`--workdir=/path/`        | Working directory (*default = submission directory*) 
`--mail-user=email@address`<br>`--mail-type=ALL` | Send mail on job events (NONE, BEGIN, END, FAIL, ALL) 

Please see the [SLURM user guide](slurm_user_guide.md) page for further details.

Don't hesitate to have also a look at the [sbatch official documentation](https://slurm.schedmd.com/sbatch.html)    


#### Script template

Just an example. Customize it to meet your needs.
```bash
#!/bin/bash

################################ Slurm options #################################

### Job name
#SBATCH --job-name=demo_job

### Limit run time "days-hours:minutes:seconds"
#SBATCH --time=01:00:00

### Requirements
#SBATCH --partition=fast
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --mem-per-cpu=8GB

### Email
#SBATCH --mail-user=email@address
#SBATCH --mail-type=ALL

### Output
#SBATCH --output=/shared/home/<user>/demojob-%j.out

################################################################################

echo '########################################'
echo 'Date:' $(date --iso-8601=seconds)
echo 'User:' $USER
echo 'Host:' $HOSTNAME
echo 'Job Name:' $SLURM_JOB_NAME
echo 'Job Id:' $SLURM_JOB_ID
echo 'Directory:' $(pwd)
echo '########################################'

# modules loading
module add ...


# What you actually want to launch
echo 'Waooouhh. Awesome.'


echo '########################################'
echo 'Job finished' $(date --iso-8601=seconds)
```



## Cluster

At your disposal:

* 68 standard compute nodes (28 cores, 256 GB RAM)  
* 1 "fat memory" compute node (64 cores, 3 TB RAM) 

Please see the [Cluster description](cluster-desc.md) page for further details.



## View results

|                          | Usage                                                  | Pros                 | Cons                 |
|:------------------------ | ------------------------------------------------------ | --------------------:| --------------------:|
| Browse data on server    | Use your favorite viewer/editor/tool: less, vim, emacs, nano, ...  | Simple. Quick. Easy  | Not always suitable, sufficient or possible.  |
| Get the data back on your workstation | See [Transfer](quick-start.md#transfer) | Have a own local copy. Flexibility (use your own tools/workstation). | Use space. Take time.  |
| Use [SSHFS](https://github.com/libfuse/sshfs) on your workstation | Browse and view your data directly from you local file manager. Integrated on many distributions. See [Transfer](quick-start.md#transfer) | Easy to browse data.  | Can be slow. Data are transferred (so it can take space and time)  |
| Export display     | Remote display, for Graphic User Interface (xemacs, geany, gedit, etc.). See [Export display](data.md#export-display)  | No data transferred (only display) | Can be slow |
| R lovers           | Use [RStudio](https://rstudio.cluster.france-bioinformatique.fr/) :) | Integrated | Specific |



## Go further

* Come discuss with us on [IFB Community](https://community.france-bioinformatique.fr/)
* Explore the documentation on [IFB Core Cluster documentation](http://taskforce-nncr.gitlab.cluster.france-bioinformatique.fr/doc/)
* Most of the softwares are public and documentation is widely available on the Internet

