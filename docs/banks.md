---
title: Public banks
---

## Shared
In order to avoid the duplication of heavy files in different private project folders, the public banks are shared between users:

`/shared/bank/`

Banks can be available in different versions and in different index format: fasta, gff, blast, bwa, bowtie, bowtie2, start, diamond, picard, ...


## Organization
The banks are organized first per species or bank names, then per index:
```
homo_sapiens/
├── GRCh38
│   ├── bwa
│   │   ├── Homo_sapiens.GRCh38.dna.primary_assembly.fa.amb
│   │   ├── Homo_sapiens.GRCh38.dna.primary_assembly.fa.bwt
│   │   └── ...
│   ├── fasta
│   │   ├── Homo_sapiens.GRCh38.dna.toplevel.fa
│   │   ├── Homo_sapiens.GRCh38.dna.toplevel.fa.fai
│   │   └── ...
│   ├── gff
│   │   ├── Homo_sapiens.GRCh38.94.gff3
│   │   └── Homo_sapiens.GRCh38.94.gff3.readme
│   └── star
│       ├── Genome
│       ├── SAindex
│       └── ...
└── hg19
    ├── bowtie
    │   ├── hg19.1.ebwt
    │   ├── hg19.2.ebwt
    │   └── ...
    ├── bowtie2
    │   ├── hg19.1.bt2
    │   ├── hg19.2.bt2
    │   └── ...
    ├── bwa
    │   ├── hg19.fa.amb
    │   ├── hg19.fa.bwt
    │   └── ...
    ├── fasta
    │   ├── hg19.fa
    │   └── hg19.fa.fai
    ├── hisat2
    │   ├── hg19.1.ht2
    │   ├── hg19.2.ht2
    │   └── ...
    ├── picard
    │   ├── hg19.dict
    │   └── ...
    └── star
        ├── Genome
        ├── SAindex
        └── ...
```

## Request a new bank, an update or an index
Please contact us using the community forum: [community.france-bioinformatique.fr](https://community.france-bioinformatique.fr/)

## Automatic updating
So far the banks aren't updated automatically with systems as [BioMaj](https://biomaj.genouest.org/).

It's still on our roadmap to manage the banks with BioMaj.
