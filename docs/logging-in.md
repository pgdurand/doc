# Logging in

Compute jobs on the NNCR HPC cluster are launched from the Unix command line. In order to access the Unix command line you must first log onto a specific computer called the login node. The process of logging on establishes a connection between your own computer and the login node.

Note that you must connect with a terminal session **at least once before using tools** that may, for example, copy files to your home directory. This is because that first connection is necessary to initialize your home directory on the HPC cluster.

## Prerequisites

To access the NNCR HPC cluster you must have
  * a valid NNCR account
  * a Secure Shell (SSH) client.

### Your NNCR HPC cluster account

In order to get your personnal NNCR HPC cluster account, please send an email to [contact-nncr-cluster@groupes.france-bioinformatique.fr](mailto:contact-nncr-cluster@groupes.france-bioinformatique.fr) using **your academic email address**

### Secure Shell client

To log on to the NNCR login node you must use the ssh (secure shell) protocol. This allows you to type commands in a window on your local computer and have them executed on the NNCR HPC cluster. To log on to the NNCR HPC cluster using ssh you must specify the fully qualified name of the login node : **core.cluster.france-bioinformatique.fr**

A SSH client is available by default on Mac or Linux computer :
* On Mac, use the Terminal app located under Applications > Utilities
* On Linux, use the Terminal or Console application available in your environment.

There is no SSH client available by default on Windows. However, you can [download PuTTY](https://the.earth.li/~sgtatham/putty/latest/x86/putty.exe) which is a free SSH client for Windows.

## Logging in to the HPC cluster

To log in to the NNCR HPC cluster, sign in to **core.cluster.france-bioinformatique.fr** using your NNCR cluster username and password.

### Mac and Linux

Open a terminal and run the ssh command:

```bash
ssh <username>@core.cluster.france-bioinformatique.fr
```


### Windows

Start Putty and enter `core.cluster.france-bioinformatique.fr` in the field Host Name

![PuTTY](imgs/putty.png)


Click the `Open` button

Your username and password will then be requested.

## Accessing your data

Several volumes of data storage are available on the NNCR cluster. All of these volumes are accessible from the `/shared` folder.

Your personal home directory (homedir) is located in `/shared/home/<your_login>`. This folder is used to store your Unix profile.

You have also access to one or more project folders to store your scientific data. These folders are located under `/shared/projects`.

If ensure, please contact [contact-nncr-cluster@groupes.france-bioinformatique.fr](mailto:contact-nncr-cluster@groupes.france-bioinformatique.fr) to know which projects folders are available for you.
