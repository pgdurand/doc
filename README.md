# Comment fonctionne la documentation ?

La documentation du cluster core de l'IFB s'appuie sur des pages au format Markdown. Ces pages sont transformés en contenu HTML à l'aide de l'outil MkDocs.

Chaque modification de la branche master du dépôt entraîne une regénération et publication automatique de la documentation.

# Comment contribuer ?

## Pour modifier une page de documentation

Cliquez sur le petit crayon en haut de la page à modifier (directement depuis le site de la documentation)

Editez le contenu de la page l'interface de GitLab.

Enregistrez vos modifications dans une nouvelle branche et créer immédiatement une MR vers la branche master.

## Pour ajouter une page de documentation

**Créer une nouvelle branche** sur le dépôt :

```bash
git checkout -b <nom-branche>
```

**Créez un fichier** à la racine du projet pour une documentation en anglais.


Enfin **poussez votre nouvelle branche sur le dépôt** et effectuer une MR vers la branche master.

```bash
git push origin <nom-branche>
```

## Tester le rendu des pages
### Pour les `docs/*`
Il faut exécuter MkDocs en local.

#### Installation
```bash
python3 -m venv ~/.venv_python3
. ~/.venv_python3/bin/activate
pip install mkdocs mkdocs-material
```

#### Usage
Placez-vous dans le dossier racine du dépôt, puis lancez
```bash
mkdocs serve
```

Le site est à présent consultable sur http://localhost:8000

### Pour les `tutorials/*`
Il faut exécuter [`claat`](https://github.com/googlecodelabs/tools/tree/master/claat) en local.

#### Installation
Du moins pour MacOSX :P
```bash
conda create -n go go
. activate go clangxx_osx-64
go get github.com/googlecodelabs/tools/claat
echo 'export PATH="/Users/lecorguille/go/bin:$PATH"' >> ~/.bashrc
claat -h
```
#### Usage
Placez-vous dans le dossier racine du dépôt, puis lancez

```bash
for tutorial in tutorials/*.md; do claat export -o site/tutorials ${tutorial}; done
```
Des mini sites seront générés dans `site/tutorials`